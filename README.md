# What is

An attempt at reversing the ProjectData format of Logic (geddit?) Pro.

## demo scripts

Mainly here to serve as documentation as my written documenting skills are pretty shit. Pass the ProjectData file as an argument, e.g.    

`python3 find_chunks.py ~/Desktop/test.logicx/Alternatives/000/ProjectData`

Some of these need the `tabulate` package (`pip3 install tabulate`)

A few different scripts :

* `find_chunks.py` dumps all the chunks, their length and offset from the beginning of the file
* `dump_all_chunk_headers.py` dumps the headers from all chunks, split into sections as loosely defined below (not even sure if these splits are correct yet)
* `dump_all_chunk_data.py` dumps the data from each chunk to a file in the current directory. This will create a lot of files! so recommended to run in a subdirectory.
* `chunk_type_counter.py` outputs the count of each chunk type. Hopefully will be useful to figure out which chunk type corresponds to various objects added/removed in the project file.  

This was undertaken using Logic 10.4.8 and 10.5.1 on OS 10.14.6.


## file format

This README is here to document the WIP, and parts of it are likely just plain wrong! My only hope is that it will become gradually less wrong! In other words, use a giant pinch of salt with **all** this info. Just imagine "maybe" in front of every single word you read. There are plenty of *actual* maybes, as you will see, so in that case, double maybe.

This investigation is focused on the `Alternatives/000/ProjectData` file inside the Logic project bundle. Other interesting files are:

* **DisplayState.plist** window positions, screensets and such. If you have an Environment window open this has a uDataEnvironment key which contains binary data
* **Autosave/DATE.songData, eventData** Could be useful but is compressed, with what I do not know
* **Undo Data** Haven't seen anything appear here yet



GARAGEBAND: Interestingly garageband projects contain two ProjectData files - one in Alternatives/000 and one in the root of the package. The first one is the same format as Logic's. The second one contains NSKeyedArchiver packed data, about 170k for a blank project. This may be worth looking into although its creation date for me is December 2019 so it may be some kind of template file that gets moved into the project bundle?

The ProjectData consists of:

* a 24-byte file header
* many "chunks", each of which has a header part (36 bytes) and a data part (length is in the header).

### File header is 24 bytes

* 4 byte magic? `23 47 C0 AB`  -- `C0 AB` is in win32 Logic 5.5 .LSO files - old!
* offset `0x04` and `0x05` are related to version?
   - `EB 06` : Logic 10.4.8 AND GB 10.3.4, wonder if these releases were concurrent
   - `D0 07` : Logic 10.5.1
   - `C5 09` : Logic 10.7.1
* `03 00 04 00 00 00 01 00 08 00`, doesn't seem to change at all.
* 64-bits little endian, combined size of all chunks

is there a checksum? You would have thought so, but havent looked into this a whole lot

-----

### chunk format

There is a 36 byte header and optionally some data.

the designations m1-m5 may not be accurate!, but scrolling through a list of all chunk headers (dump.txt) these splits look to be a reasonable assumption for now.

I think m2-m5 are addresses of some kind, but not all of them are needed for a particular chunk type. I think an address will get set to `FF FF FF FF` if it's not needed. See m*x* address field speculation table below.

| offsef   | bytes | (my) name | Desc |
|---                |---|---    |---    |
|`0x00` - `0x03`    |4  |descriptor   |four char descriptor, backwards - ex. `69 76 6E 45` is "ivnE" -> "Envi" for environment object.|
|`0x04` - `0x05`    |2  |m1   | 16-bit int, probably. was hoping this would describe which of these other fields to use but no such luck|
|`0x06` - `0x09`    |4  |m2   | 32-bit int `FF FF FF FF` in the song chunk only, otherwise its a number|
|`0x0A` - `0x0D`    |4  |m3/OID| 32-bit int this apears to be a unique ID, maybe just uniue within the particular chunk type though. connecting env objects together includes this at the end of the data part of the obj making the connection. `FF FF FF FF` in the song chunk only.|
|`0x0E` - `0x11`    |4  |m4   | 32-bit int sometimes `FF FF FF FF`, sometimes something else|
|`0x12` - `0x15`    |4  |m5   | 32-bit int sometimes `FF FF FF FF`, sometimes `FF FF FF 7F`, sometimes a normal looking LE 32-bit|
|`0x16` - `0x1B`    |6  |stati   | `02 00 00 00 01 00` this is always the same, see dump.txt|
|`0x1C` - `0x23`    |8  |len | 64-bit int length of chunk data |
|`0x24` - end       |len|data|chunk data itself, length described by previous field, nonexistent if len==0|

-----
## Chunk descriptors
There are relatively few chunk types given the complexity of a project, so there will be some subtypes to be discovered (Environment objects etc).

* `gnoS` -> `Song` occurs once at the beginning of the file. Always 3176 bytes??
* `qeSM` -> `MSeq` marker sets, midi regions?!!?!?
* `qSvE` -> `EvSq` "Event Sequence" - markers, automation. Looks to contain the actual automation data. (see AuEv below). Automation curves/ramps look to be "rendered" to the save file as they make for MUCH bigger EvSq chunks than instantaneous changes. One chunk per track.
* `karT` -> `Trak`
* `tSxT` -> `TxSt` looks like it contains font ("Times") and musical notation info ("Tuplets", "Mult Rests" etc), also `abcABC123456` in every chunk, prob uninitialized string
* `snrT` -> `Trns` MIDI Transform presets? appears when software instrument stuff is added to proj
* `lFuA` -> `AuFl` references to audio files, think unicode strings
* `gRuA` -> `AuRg` think this is audio regions
* `nCuA` -> `AuCn` not a lot of readable info here, it's all binary and interspersed with `AuCO` - maybe this is sends or something?
* `OCuA` -> `AuCO` audio related, lots of these, one for each input, output, bus blah blah blah
* `UCuA` -> `AuCU` plugin stuff, contains bplist00. More of these show up when channel inserts are added
* `vEuA` -> `AuEv` appears when track automation is added to the project. Each parameter automated -> 1 more chunk
* `tScS` -> `ScSt` Score styles?? Big (3.7k) and stuffed with group text, colour text, and notation stuff
* `lytS` -> `Styl` also score styles? maybe a single style? Don't know anything about logic scoring
* `tSnI` -> `InSt` more score stuff?
* `ryaL` -> `Layr` Environment layers. See below
* `ediV` -> `Vide` presumably something to do with video track, still present if no video is in the proj
* `OgnS` -> `SngO` look to contain bplist00 data, something to do with drummer feature & loops?
* `MroC` -> `CorM` Contains the strings "MIDI Mix" and the name of a MIDI input device. not sure what this is
* `rpyH` -> `Hypr` maybe this refers to the old "hyper edit". Is this for region based automation?
* `ivnE` -> `Envi` Stuff in the environment, but also tracks in the Arrange. not sure how this relates yet.
* `qSxT` -> `TxSq` marker info, this is usually at the end of the file, in [rich text format](https://docs.fileformat.com/word-processing/rtf/) this may also be used for project notes

## different chunk types' use of the still imaginary "m*x*" fields!

|type 	|m1    |m2       |m3     |m4     |m5     | len |
|---  	|---   |---      |---    |---    |---    |---
|`Song`	|3     |`FF FF FF FF`     | `FF FF FF FF`  |`FF FF FF FF`   |`FF FF FF FF`   |      |
|`MSeq`	|2     | dups         | dups      |`FF FF FF FF`  |`FF FF FF FF`   |    |
|`EvSq`	|1     | matches `MSeq`  | matches `MSeq` | uniq | `FF FF FF FF` |   |
|`Trak`	|4     | `17`   | dups | `FF FF FF FF` | `FF FF FF F7` or uniq, some dups of `00` | 0 or 56 |
|`TxSt`	|1     | `04 00 00 00` | uniq (inc by 4) | `FF FF FF FF` | `FF FF FF FF` | about 150 (prob string length dep) |
|`Trns`	|
|`AuFl`	|1      | `0B 00 00 00` | uniq (inc by 4) | `FF FF FF FF` | `FF FF FF FF` |
|`AuRg`	|1      | `0B 00 00 00` | groupd (inc by 4) | 0, 1, 2, 3 ... for each m2 | `FF FF FF FF` |
|`AuCn`	|1      | `0E 00 00 00` | uniq (inc by 4) | `FF FF FF FF` | `FF FF FF FF` | 132 OR one  1356!! |
|`AuCO`	|2      | `0E 00 00 00` | `24` = bus? some uniq | uniq for each val of m2? | `FF FF FF FF` | bus=132  string length dep |
|`AuCU`	|2      | `0E 00 00 00` | `24 00 00 00` | dups x6 | uniqs for each m4| wildly varied |
|`ScSt`	|1      | `0E 00 00 00` | `0C 00 00 00` | only one of these | only one of these | 3752 |
|`Styl`	|1      | `0C 00 00 00` | uniq (inc by 4) | `FF FF FF FF` | `FF FF FF FF` | 100-280 in big proj |
|`InSt`	|1      | `02 00 00 00` | uniq (inc by 4) | `FF FF FF FF` | `FF FF FF FF` | couple of hundo |
|`Layr`	|1      | `0E 00 00 00` | `08 00 00 00` | `FF FF FF FF`   | `FF FF FF FF` | dep on number of layers
|`Vide`	|3      | `0E 00 00 00` | `18 00 00 00` | `FF FF FF FF`   | `FF FF FF FF` | only one of these i think |
|`SngO`	|1      | `0E 00 00 00` | notsure       | `FF FF FF FF`   | `FF FF FF FF` | 1.1k or 186k!?!?! |
|`CorM`	|1      | `0E 00 00 00` | uniq?         | `FF FF FF FF`   | `FF FF FF FF` | 1606? |
|`Hypr`	|1      | `0F 00 00 00` | uniq (inc by 4) | `FF FF FF FF` | `FF FF FF FF` | var|
|`Envi`	|5      | `14 00 00 00` | uniqs          | `FF FF FF FF`  | `FF FF FF FF` | var |
|`TxSq`	|1      | `20 00 00 00` | uniqs         | `FF FF FF FF`  | `FF FF FF FF` | var |

------
## specific chunk data investigation
A few investigations now, I haven't had time to delve into all these things in detail obviously. They were just the few things I was playing around with and seeing what difference it made in the resulting file.  
Hex addresses mentioned below are offsets from the beginning of the data part of the chunk (36 bytes after the chunk descriptor)

the one common thing between all the chunk data sections is the first 16 bytes, they seem to be some kind of clue of where the rest of the data is in the chunk.

Can't see any checksums at this level yet.

### `Layr` - A list of environment layers

This is among the least "busy" type of chunk and probably the easiest to understand completely, so let's have a go. Hoping it will shed some light on other more complex chunks.

first 16 bytes:
* first byte cycles `00 -> 20 -> 40 -> 80 -> A0 -> C0 -> E0 -> 00` etc when new layers are added
* second byte increases by 2 each time new layer is added (maybe counting the total of longname+shortname fields). When first byte cycles from `E0` back to `00`, this value increases by 3 but no extra fields are added. Weird. The "look" of this 16 byte field is shared by other types of chunk, and sometimes has other data in it, so I'm probably just not looking at it right.
* rest of these bytes are `00` probably
* Similar to a lot of environment objects, there is a short and a long name.
* this is very annoying to figure out since "create layer" is bugged and sometimes erases whole swathes of layers when adding >16 layers

#### shortname (16 bytes fixed length)

* always starts with space `0x20` which is presumably ignored when read by Logic
* 14 characters
* null terminator

#### longname (16-bit length field) + (string) + padding

* length field is 16-bit LE
* string is ascii characters (no `0x20` at start)
* padding: single `00` for odd string lengths only
* padding2: 16 bytes of `00`. Doesn't exist if this is the last layer in the chunk.


### `Envi` - investigating environment objects

Seems to be split into a "generic" and "custom" section. Generic stuff are parameters common to all objects - name, position, icon, that kind of thing. Fixed length data for everything except the long name string. "custom" is more groovy parameters that not all objects have, i.e. MIDI channel, transformer paramers, log of past MIDI Activity, stuff like that

I'm not sure whether the split point between these two as an offset is available anywher in the chunk, hardcoded into the app or calculated from long name len + generic section len.

#### "generic" section


| offset            | len     |  Desc   |
|---                |---      |---      |
| `0x00` - `0x0F`   | 16      | some kind of length, First two bytes apear to be related to length of the custom section. |
| `0x10` - `0x1F`   | 16      | related to environment object id |
| `0x20` - `0x4F`   | 48      | not known |
| `0x50`            | 1       | object is highlighted/selected. `00` = no, `01` = yes |
| `0x51`            | 1       | object type, see `dump_env_objects.py` for details |
| `0x52` - `0x53`   | 2       | X position |
| `0x54` - `0x55`   | 2       | Y position |
| `0x56` - `0x57`   | 2       | width |
| `0x58` - `0x59`   | 2       | height |
| `0x5A` - `0x5B`   | 2       | layer index, gona assume this is 16bit for now but could be 32 as next 4 bytes are `00`, see `Layr` section |
| `0x5C` - `0x5F`   | 4       | always `00 00 00 00` |
| `0x60` - `0x6F`   | 16      | gonna assume 16 bytes, always `08` then 15x `00`|
| `0x70` - `0x71`   | 2       | always `00 00` |
| `0x72` - `0x73`   | 2       | same as bytes `0x00` and `0x01` , some kind of length or is it a marker to find the custom section? |
| `0x74` - `0x93`   | 32      | short name, always starts with a space, then 29 chars, then (check this ->>) `00 00` at end |
| `0x94` - `0x95`   | 2       | icon |
| `0x96` - `0x99`   | 4       | `60 00 00 00`, `00 00 00 00` stuff like that |
| `0x9A`            | 1       | assignable? `01` = yes, `00` = no, ch strips go to `08` `09` when muted? this could be a bitfield |
| `0x9B`            | 1       |       |
| `0x9C`            | 1       |       |
| `0x9D`            | 1       |       |
| `0x9E` - `0x9F`   | 2       | long name length |
| `0xA0` ----->     | var     | long name, if length is odd the end has a single `00` padding |

notes:
* `0x00` to `0x0F` First two bytes apear to be related to length of the custom section. they change (+4) when 4 bytes added to the connection list, but do not change when the length of the long name changes.
  - with no connections, ornament = `E8 02` (744), voice limiter = `03 78` (888)
  - 888-744 = 144 which is the difference in size between the two objs
  - is there a "base size" which we need to subtract ?? 888 is too big (whole data part is only 342)
* `0x10` to `0x1F` possibly related to an env obj ID #. This number increases by 4 in each new object created. IDs released by deleted objects are used first, e.g. if you have objs `A0` `A4` `A8` `AC` and you delete the object with `A8`, `A8` will be the id# of the next object you create. This is the same as m3/OID in the header.

#### "custom" section

* offset `0xA0` + [long name length] + [optional padd] + 1 : 16LE? this changes with the bus number assigned to a channel strip, if u assign channel strip to be a bus, it's one more than the m3 value of the bus AuCO chunk
* connection list is at the end of the data chunk - not sure how to offset to here from the rest of the chunk. Maybe just work backwards from the end? REWIND
   - 4 bytes added every time a connection is made from this obj
   - bytes 1+2 are the target's OID (16bit LE)
   - byte 3 is `01` or `00`, have seen this chagne when target selected/deselected, also possibly changes when target is moved around the layer? Maybe moved to another layer?
   - byte 4 have seen `00` `05` `F4` `6D` -> `70` with subsequent connections, maybe this is offset
* monitor object is a lot bigger than ornament, seems to contain a short history of the data that went thru. same deal for keyboard.

### investigating markers

* Appears as two chunks: `EvSq` and at the end of the file `TxSp`
* an `EvSp` chunk contains the position data??? may be wrong about this
* `TxSp` is rich text. Think the actual marker name is taken from the first line only
* the chunk length is repeated a few times in the *data* part of `TxSp`, not sure why
* need to find how the marker text is linked to its position chunk

### investigating audio tracks

The audio object itself has an `AuCO` chunk. The track(s) has an `Envi` chunk, which presumably refers back to the `AuCO`

* `Envi` chunk type
* `0x74` = start of null terminated short name, max 31 chars? space is inserted at start
* `0x9A` = mute `80`=muted, `88`=open (now have been a bit deeper into `Envi`, check this again)

### `AuCO` (**Au**dio **C**hannel **O**bject??)

* offset `0x74`: 32bit LE int fader level. `00 00 00 00`is -inf and `00 00 00 7F` is +6.0 so  dB = 40 * log10 (val/1509949440) then round to one decimal place
* 0ffset `0x55`: the MSB of the 32bit value above. dB = 40 * log10 (val/0x5A)
* `0x56`: 0=mono 1=stereo 2=L 3=R 4=Surround
* `0x59`: stereo pan, 00=L 40=C 7F=R
* offset `0x80`: gets set to `01` when send activated? can't see any evidence of sends here
* `0x5C` `0x5D`: no output=`FF FF`, otherwise output number (Stereo out = `00 00`)
* `0x5E` `0x5F`: no input=`FF FF`, otherwise input number (input 1 = `00 00`)

### `AuCU` (**Au**dio **CU**e (AUX)??)

seems t work in tandem with AuCO, this has the info for bus sends

### `Song`

* offset `0x8D`: active marker set

### `EvSq` Marker

* each marker uses 48 bytes
* each marker data begins with `12 00 00 00`
* then 32?-bit ticks, relative to position -9 1 1:
  - -9 1 1 is `00 00 00 00`
  - 1 1 1  is `00 96 00 00` (10 bars * 3840 ticks per bar = 38400)
  - 1234 1 2 is `01 D5 48 00`
  - 8 1 772 is `03 02 01 00`  
  these last two are magic values I can use to locate the marker chunk without knowing the proper way to do it
* `00 00 00 00` (ticks might actually be 64 bit then)
* `00 00 00 xx` xx = `0x00` `0x01` or `0x81`, duno what this is, looks flaggy and flips around quite a lot between saves
* 32 bit value, this corresponds to the m3 value of the `TxSq` chunk containing the marker's rtf.
* `00 00 xx 88` xx is 00 for standard marker, 01 for scene marker
* `00 00 00 00` const?
* 32-bit marker length in ticks
* `00 00 00 00 00 00 00 88 00 00 00 00 00 00 00 00`
* an additional 32 bytes are added when marker is either a scene marker or a SMPTE-locked standard marker
* `xx xx xx xx 00 00 00 BE 00 00 00 00 00 00 00 00` xx=marker SMPTE position in subframes, 0xBE=192, don't know why it's there
* `xx xx xx xx 00 00 00 BE 00 00 00 00 00 00 00 00` xx=marker SMPTE length in subframes, this only changes when the marker is unlocked and relocked. Assume this is calculated from the marker length in beats/bars??


* marker chunk (how to id?) ends with `xx 00 00 00 FF FF FF 3F 00 00 00 00 00 00 00 00` in a marker chunk with no markers, this is the only thing in the chunk (length=0x10). xx is F1 but then changes to 00?


moving the marker to another set only changes the m3 pointer, maybe the marker set is referenced from the `TxSq` chunk?




### investigating MIDI regions

* `MSeq` chunk type
* `0x10` `0x11` 16 bit LE int, long region name [length]
* `0x12` - long region name [length] bytes
* short region name is a field of 32 bytes, with a mandatory `0x20` at the start and a null terminator leaving 30 bytes for the name itself
* `0xFC` - `0xFF` appears to be start position in ticks (960 per beat) think this is 32bit LE
* `0x58` - `0xFB` region legnth in ticks
* `0x25` area - related to colour
