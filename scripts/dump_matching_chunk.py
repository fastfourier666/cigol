# take two projectdata files and output any chunks that are unique to the second one

from pathlib import Path
import sys
import os

fileName = sys.argv[1]


class Chunk:
    def __init__ (self, header, data):
        self.header = header
        self.data = data

        self.type = self.header[0:4].decode()[::-1]

        #        --?-- ---??????-- ---OID??---         type/subtype???       always the same--  ---size of chunk data--
        # MSeq   02 00 03 00 00 00 00 00 00 00    ff ff ff ff ff ff ff ff    02 00 00 00 01 00  2c 01 00 00 00 00 00 00 (300 bytes)

        self.m1 = int.from_bytes(self.header[4:6], "little", signed=False)      # 02 00 above, think this is 16 bit int
        self.m2 = int.from_bytes(self.header[6:10], "little", signed=False)     
        self.m3 = int.from_bytes(self.header[10:14], "little", signed=False)    # there is correlation here in environment objs
        self.m4 = int.from_bytes(self.header[14:18], "little", signed=False)    # sometimes a number, sometimes f
        self.m5 = int.from_bytes(self.header[18:22], "little", signed=False)    # mostly ff ff ff ff sometimes ff ff ff 7f

        chunkSizeBytes = self.header[28:36]                # pull the size (64 bit unsigned int)
        chunkSize=int.from_bytes(chunkSizeBytes, "little", signed=False) # convert to int
    def __str__ (self):
        return  ("its shit")

    def __repr__ (self):
        return  self.header[0:4].decode()[::-1]

def readFile (fileName, chunkDestination):
    # print (fileName)
    with open(fileName, mode='rb') as file: 
        fc = bytearray (file.read())                    # read entire file into byte array
        filesize = os.path.getsize(fileName)            # get size in bytes from OS
        # print ("File is %d 0x%X bytes (from OS)" %(filesize, filesize))

        fp=0x18         # move the file pointer 24 bytes, skipping over the entire header
        found_count=0

        while (fp<filesize):
        # while (found_count < 10):
            chunkStart=fp
            chunkHeader = fc [fp:fp+36]                 # byte array, whole chunk header
            chunkSizeBytes = chunkHeader[28:36]                # pull the size (64 bit unsigned int)
            chunkSize=int.from_bytes(chunkSizeBytes, "little", signed=False) # convert to int
            fp+=36  # fp now points at the frst byte of the chunk's data
            chunkData = fc [fp:fp+chunkSize] # pull the chunk's data
            fp += chunkSize                 # skip over all the chunk's data, should be at 1st byte of next chunk descriptor now
            nc = Chunk(chunkHeader, chunkData)
            chunkDestination.append (nc)
            found_count +=1

        # print ("%d chunks found" % (found_count))


chunks=[]
foundchunks=[]
matching=0
toplength=0
readFile (fileName,chunks)


print ("1: %s\t%d chunks" % (Path(fileName).name, len(chunks)))
print ("looking for %s" % sys.argv[3])


seek = bytearray (sys.argv[3], "ascii")
for c in chunks:
    if (c.data.find(seek,0) != -1):
        foundchunks.append(c)
        matching+=1
        if (len(c.data) > toplength):
            toplength = len(c.data)


print ("%d matches found for %s" % (matching, sys.argv[3]))
# print ("longest chunky is %s bytes, padding len= %d" % (toplength ,len(padding)))

# wrte them out in a more readable/diffable way
outFile = sys.argv[2]
newFile = open (outFile, "wb")
for c in foundchunks:
    # newFile.write (c.header)
    # newFile.write (bytearray([0xdd] * 12))      # i like to look at this in 16 byte widths, so pad
    newFile.write (c.data)

    totallen = len(c.header) + len(c.data)
    paddinglen = 32-(totallen % 32)+512
    padding = bytearray ([0x88] * paddinglen)
    print ("chunky is %s bytes, padding len= %d" % (totallen ,len(padding)))
    # newFile.write (padding)
newFile.close
