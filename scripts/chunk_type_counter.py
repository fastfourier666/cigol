# count the number of each type of chunk

import sys
import os
from collections import Counter

fileName = sys.argv[1]
print (fileName)
with open(fileName, mode='rb') as file: 
    fc = bytearray (file.read())                    # read entire file into byte array
    filesize = os.path.getsize(fileName)            # get size in bytes from OS
    print ("File is %d 0x%X bytes (from OS)" %(filesize, filesize))

    fp=0x18         # move the file pointer 24 bytes, skipping over the entire header
    found_count=0

    chunkTypes = []

    while (fp<filesize):
        chunkStart=fp

        chunkHeader = fc [fp:fp+36]                 # byte array, whole chunk header
        chunkName = chunkHeader[0:4].decode()[::-1]
        chunkNameHex = chunkHeader [0:4]
        chunkTen = chunkHeader[4:14]
        chunkF = chunkHeader[14:22]
        chunkStatix = chunkHeader[22:28]

        chunkSizeBytes = chunkHeader[28:36]                # pull the size (64 bit unsigned int)
        chunkSize=int.from_bytes(chunkSizeBytes, "little", signed=False) # convert to int


        # print ("%s   %s    %s    %s  %s (%d bytes)" % (chunkName, chunkTen.hex(sep=' '), chunkF.hex(sep=' '), chunkStatix.hex(sep=' '), chunkSizeBytes.hex(sep=' '), chunkSize))
        
        fp+=36  # fp now points at the frst byte of the chunk's data
        chunkData = fc [fp:fp+chunkSize] # pull the chunk's data

        # we do nothing with the chunk data for now

        fp += chunkSize                 # skip over all the chunk's data, should be at 1st byte of next chunk descriptor now
        chunkTypes.append (chunkName)   # add the chunk name to the list

        found_count +=1

    print ("%d total chunks found" % (found_count))
    counter_object = Counter(chunkTypes)
    keys = counter_object.keys()
    num_values = len(keys)
    print("%d different chunk types found" %(num_values))
    for n in keys:
        print ("%s  %s" % (n, counter_object [n]))
