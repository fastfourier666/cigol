# take two projectdata files and output any chunks that are unique to the second one

from pathlib import Path
import sys
import os

fileName1 = sys.argv[1]
fileName2 = sys.argv[2]

class Chunk:
    def __init__ (self, header, data):
        self.header = header
        self.data = data

        self.type = self.header[0:4].decode()[::-1]

        #                                         type/subtype???            always the same--  ---size of chunk data--
        # MSeq   02 00 03 00 00 00 00 00 00 00    ff ff ff ff ff ff ff ff    02 00 00 00 01 00  2c 01 00 00 00 00 00 00 (300 bytes)

        self.m1 = int.from_bytes(self.header[4:6], "little", signed=False)      # 02 00 above, think this is 16 bit int
        self.m2 = int.from_bytes(self.header[6:10], "little", signed=False)     
        self.m3 = int.from_bytes(self.header[10:14], "little", signed=False)
        self.m4 = int.from_bytes(self.header[14:18], "little", signed=False)    # sometimes a number, sometimes f
        self.m5 = int.from_bytes(self.header[18:22], "little", signed=False)    # mostly ff ff ff ff sometimes ff ff ff 7f

        chunkSizeBytes = self.header[28:36]                # pull the size (64 bit unsigned int)
        chunkSize=int.from_bytes(chunkSizeBytes, "little", signed=False) # convert to int
    def __str__ (self):
        return  ("its shit")

    def __repr__ (self):
        return  self.header[0:4].decode()[::-1]

def readFile (fileName, chunkDestination):
    # print (fileName)
    with open(fileName, mode='rb') as file: 
        fc = bytearray (file.read())                    # read entire file into byte array
        filesize = os.path.getsize(fileName)            # get size in bytes from OS
        # print ("File is %d 0x%X bytes (from OS)" %(filesize, filesize))

        fp=0x18         # move the file pointer 24 bytes, skipping over the entire header
        found_count=0

        while (fp<filesize):
        # while (found_count < 10):
            chunkStart=fp
            chunkHeader = fc [fp:fp+36]                 # byte array, whole chunk header
            chunkSizeBytes = chunkHeader[28:36]                # pull the size (64 bit unsigned int)
            chunkSize=int.from_bytes(chunkSizeBytes, "little", signed=False) # convert to int
            fp+=36  # fp now points at the frst byte of the chunk's data
            chunkData = fc [fp:fp+chunkSize] # pull the chunk's data
            fp += chunkSize                 # skip over all the chunk's data, should be at 1st byte of next chunk descriptor now
            nc = Chunk(chunkHeader, chunkData)
            chunkDestination.append (nc)
            found_count +=1

        # print ("%d chunks found" % (found_count))


chunks1=[]
chunks2=[]

readFile (fileName1,chunks1)
readFile (fileName2,chunks2)

print ("1: %s\t%d chunks" % (Path(fileName1).name, len(chunks1)))
print ("2: %s\t%d chunks" % (Path(fileName2).name, len(chunks2)))

outFile = "results.bin"
newFile = open (outFile, "wb")

# for each chnk in file1, try to find a matching one in file2 

diffs=0
for c1 in chunks1:
    bFound=False
    for c2 in chunks2:
        if c1.data == c2.data:
            bFound=True
    if not bFound:
        # print ("diff ")

        if (c1.type() != "Song"):
            newFile.write (c1.header)
            newFile.write (c1.data)
        diffs+=1


newFile.close
print ("%d diffs" % (diffs))
