# dump all the chunk headers

from pathlib import Path
from tabulate import tabulate
import sys
import os

fileName = sys.argv[1]


class Chunk:
    def __init__ (self, header, data):
        self.header = header
        self.data = data

        self.type = self.header[0:4].decode()[::-1]

        #        --?-- ---??????-- ---OID??---         type/subtype???       always the same--  ---size of chunk data--
        # MSeq   02 00 03 00 00 00 00 00 00 00    ff ff ff ff ff ff ff ff    02 00 00 00 01 00  2c 01 00 00 00 00 00 00 (300 bytes)

        self.m1 = int.from_bytes(self.header[4:6], "little", signed=False)      # 02 00 above, think this is 16 bit int
        self.m2 = int.from_bytes(self.header[6:10], "little", signed=False)     
        self.m3 = int.from_bytes(self.header[10:14], "little", signed=False)    # there is correlation here in environment objs
        self.m4 = int.from_bytes(self.header[14:18], "little", signed=False)    # sometimes a number, sometimes f
        self.m5 = int.from_bytes(self.header[18:22], "little", signed=False)    # mostly ff ff ff ff sometimes ff ff ff 7f

        chunkSizeBytes = self.header[28:36]                # pull the size (64 bit unsigned int)
        self.chunkSize=int.from_bytes(chunkSizeBytes, "little", signed=False) # convert to int
    def __str__ (self):
        return  ("its shit")

    def __repr__ (self):
        return  self.header[0:4].decode()[::-1]

def readFile (fileName, chunkDestination):
    # print (fileName)
    with open(fileName, mode='rb') as file: 
        fc = bytearray (file.read())                    # read entire file into byte array
        filesize = os.path.getsize(fileName)            # get size in bytes from OS
        # print ("File is %d 0x%X bytes (from OS)" %(filesize, filesize))

        fp=0x18         # move the file pointer 24 bytes, skipping over the entire header
        found_count=0

        while (fp<filesize):
        # while (found_count < 10):
            chunkStart=fp
            chunkHeader = fc [fp:fp+36]                 # byte array, whole chunk header
            chunkSizeBytes = chunkHeader[28:36]                # pull the size (64 bit unsigned int)
            chunkSize=int.from_bytes(chunkSizeBytes, "little", signed=False) # convert to int
            fp+=36  # fp now points at the frst byte of the chunk's data
            chunkData = fc [fp:fp+chunkSize] # pull the chunk's data
            fp += chunkSize                 # skip over all the chunk's data, should be at 1st byte of next chunk descriptor now
            nc = Chunk(chunkHeader, chunkData)
            chunkDestination.append (nc)
            found_count +=1

        # print ("%d chunks found" % (found_count))


chunks=[]
readFile (fileName,chunks)
print ("ProjectData: %s\t%d chunks" % (Path(fileName).name, len(chunks)))

t=[]
for c in chunks:
    t.append ([c.type, f'{c.m1:04X}', f'{c.m2:08X}', f'{c.m3:08X}', f'{c.m4:08X}', f'{c.m5:08X}', f'{c.chunkSize}'])
    # t.append ([f'{objID:X}', longName, layers[layer], f'{posX},{posY}',f'{width}x{height}', oTypeBytes.hex(sep=' '), f'{byte0x51:02X}'])

print (tabulate(t, headers=["type ","m1", "m2", "m3", "m4", "m5", "len"],tablefmt="pretty", stralign="left"))
