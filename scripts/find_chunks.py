
import sys
import os

fileName = sys.argv[1]
print (fileName)
with open(fileName, mode='rb') as file: 
    fc = bytearray (file.read())                    # read entire file into byte array
    filesize = os.path.getsize(fileName)            # get size in bytes from OS
    print ("File is %d 0x%X bytes (from OS)" %(filesize, filesize))

    fp=0x18         # move the file pointer 24 bytes, skipping over the entire header
    found_count=0

    while (found_count<1):
        chunkStart=fp

        chunkHeader = fc [fp:fp+36]                 # byte array, whole chunk header

        chunkName = fc[fp:fp+4].decode()[::-1]
        chunkTen = fc[fp+5: fp+15]
        chunkF = fc[fp+16: fp+23]
        chunkStatix = fc[fp+24:fp+29]

        print (dump(chunkHeader))

        chunkSizeBytes = fc[fp+30:fp+8]                # pull the size (64 bit unsigned int)
        chunkSize=int.from_bytes(chunkSizeBytes, "little", signed=False) # convert to int
        fp += 8                         # advance to the start of the chunk's data
        chunkData = fc [fp:fp+chunkSize] # pull the chunk's data



        # we do nothing with the chunk data for now

        print ("chunk type=%s\t offset=0x%X\t length=%d " % (chunkName, chunkStart, chunkSize))

        fp += chunkSize                 # skip over all the chunk's data, should be at 1st byte of next chunk descriptor now
        found_count +=1

    print ("%d chunks found" % (found_count))

def dump(src, length=8):
    N=0; result=''
    while src:
       s,src = src[:length],src[length:]
       hexa = ' '.join(["%02X"%ord(x) for x in s])
       s = s.translate(FILTER)
       result += "%04X   %-*s   %s\n" % (N, length*3, hexa, s)
       N+=length
    return result